<?php

$nome = "Ana Caroline";
echo $nome;

$idade = 27;
echo "<br>";
echo $idade;

$salario = 1000.50;
echo "<hr>";
echo "R$ ". number_format($salario,2,",",".");

echo "<hr>";

$nome = "Ana";
$sobrenome = "Ferreira";
echo $nome ." ". $sobrenome;

/*
pode utilizar também:

echo "$nome $sobrenome";

echo "Meu nome é: $nome e o sobrenome é: $sobrenome";
*/

echo "<hr>";

echo 'A caixa d\'agua está vazia';
//ou "A caixa d'agua está vazia"

echo "<hr>";

//Aula de "PHP"
echo 'Aula de "PHP"';

?>