<?php

    $nome = 'Ana Ferreira';
    $idade = 39;
    $email = 'ferreirinha@e-mai.com';
    $senha = '12345678';
    $cursos = ['PHP', 'HTML', 'CSS'];

    echo '<h1> Trabalhando com Estrutura condicional</h1>';

    echo '<h2>Exemplo de if (se...)</h2>';

    if($idade >= 18){
        echo "O usuário $nome é maior de idade";
    }

    echo "<hr>";

    ########################################################

    echo '<h2>Exemplo de if Ternário</h2>';

    echo ($idade >= 18) ? "Maior de Idade" : "Menor de idade";

    ########################################################

    echo "<hr>";
    echo "<h2>Exemplo de if e else</h2>";

    if($email == 'ferreirinha@e-mai.com' && $senha == '12345678'){
        echo "Usuário logado";
    }else{
        echo "Usuário ou Senha Inválida";
    }

    #######################################################

    echo "<hr>";
    echo "<h2>Exemplo login de if e else</h2>";

    if($email == 'ferreirinha@e-mai.com'){
        if ($senha == '12345678'){
            echo "Usuário logado";
    }else{
        echo "Usuário ou Senha Inválida";
    }
    }

    #######################################################
    echo "<hr>";
    echo "<h2>Exemplo de Multiplas Condições</h2>";

    $num1 = 10;
    $num2 = 20;

    if($num1 == $num2){
        echo "Os números são iguais";
    } else if ($num1 > $num2) {
        echo "O número 1 é maior que o número 2";
    } else {
        echo "O número 2 é maior que o número 1";
    }

    #########################################################

    echo "<hr>";
    echo "<h2>Exemplo de GET</h2>";

    $menu = $_GET['menu'] ?? "Home";

    switch(strtolower($menu)){
        case "home":
            echo "Página Principal";
            break;
        case "Empresa":
            echo "Página Empresa";
            break;
        case "Produtos":
            echo "Página Produtos";
            break;
        case "Contato":
            echo "Página Contato";
            break;
        default:
            echo "Página erro 404";
    }

?>