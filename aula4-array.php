<?php

#Array Unidimencional

$listaCompra = ['Arroz','Feijão','Banana','Detergente','Sabonete'];

$listaCompra [] = "Ração PET";

echo "<pre>";
print_r($listaCompra);
echo "</pre>";

echo "<hr>";

var_dump($listaCompra);

echo "<hr>";

echo $listaCompra[2];

echo "<hr>";

echo $listaCompra[4].", ".$listaCompra[2];

echo "<hr>";

foreach ($listaCompra as $item) {
    echo $item;
    echo "<br>";
}

echo "<hr>";

#Array Associativo

$funcionario = [
    "nome" => "Ana Ferreira",
    "cargo" => "MEP",
    "idade" => 27,
    "salario" => 1500.50,
    "ativo" => true
];

var_dump($funcionario);

echo $funcionario["cargo"];

echo "<hr>";

#Array Multidimensional

$funcionarios = [
    [
        "nome" => "Ana Ferreira",
        "cargo" => "MEP",
        "idade" => 27,
        "salario" => 1500.50,
        "ativo" => true,
        "cursos" => ["WEB", "PHP", "Javascript"]
    ],

    [
        "nome" => "João",
        "cargo" => "MEP",
        "idade" => 45,
        "salario" => 2000.00,
        "ativo" => false,
        "cursos" => []
    ],

    [
        "nome" => "Lurdes",
        "cargo" => "MEP",
        "idade" => 50,
        "salario" => 1500.50,
        "ativo" => true,
        "cursos" => ["Photoshop", "Illustrator"]
    ]
];

var_dump($funcionarios);

echo $funcionarios[1] ['nome'];

echo "<br>";

echo $funcionarios[0] ['cursos'] [1];

echo "<br>";

echo "<p>";
echo "Nome: ", $funcionarios[2] ['nome'];
echo "<br>";
echo "Cargo: ", $funcionarios[2] ['cargo'];
echo "<br>";
echo "Curso: ", $funcionarios[2] ['cursos'] [0];                                             
echo "</p>";

echo "<hr>";
echo "<hr>";

foreach ($funcionarios as $item) {
    echo "Nome: ".$item["nome"];
    echo "<br>";
    echo "Cargo: ".$item["cargo"];
    echo "<br>";
    echo "Idade: ".$item["idade"];
    echo "<hr>";
    echo "Cursos: ".$item["idade"];
    echo "<hr>";
}