<?php

$pessoa = [
    1 => [
        'nome' => 'João Batista',
        'cargo' => 'Professor',
        'competencia' => [
            'informatica' => ['word', 'excel'],
            'formacao' => ['Analista de Sistema', 'Pós em Desenvolvimento de Software'],
        ]
        ],


2 => [
    'nome' => 'Maria dos Santos',
    'cargo' => 'Enfermeira',
    'competencia' => [
        'informatica' => ['word', 'excel', 'power point', 'internet'],
        'formacao' => [
            'Enfermeira' => 'Especialista Centro Cirurgico'
        ],
        'Pós em Urgencia'
    ]
    ],
];

var_dump($pessoa);

echo $pessoa[1]['competencia']['formacao'][1];
echo "<br>";
echo $pessoa[2]['competencia'][0];
echo "<br>";
echo $pessoa[2]['competencia']['formacao']['Enfermeira'];
