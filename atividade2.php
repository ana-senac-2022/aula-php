<?php
    $nome = "Ana Ferreira";
    $cnpj = "94.600.382/0001‐00";
    $rg = "34.200.555";
    $cpf = "111.333.222‐33";
    $endereco = "R.Paraíba, 125 ‐ Marília, SP, 17509‐060";
    $empresa = "Senac";
    $cargo = "Docente";

    ?>

<!DOCTYPE html>

<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Atividade</title>
</head>

<body>

    <h1>DECLARAÇÃO DE LOCAL DE TRABALHO</h1>

    <p>
        Eu, <?=$nome ?>, brasileiro, <?=$cargo ?>, inscrito(a) no CPF sob o nº <?=$cpf ?> e no RG nº <?=$rg ?>, declaro para os devidos fins que possuo vínculo empregatício com a empresa <?=$empresa ?> inscrita no CNPJ sob o nº <?=$cnpj ?>, localizada à <?=$endereco ?>.
    </p>

    <p>
        Por ser expressão da verdade, firmo apresente para efeitos legais.
    </p>

    <br>

    <p>
        Marília–SP, 15 de Setembro de 2022
    </p>

    <br>
    <br>

    <p>
        <?=$nome ?>
    </p>
    
</body>

</html>