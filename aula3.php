<?php

/*

#camelCase
#snack_case

Tipos de Variaveis

string
number (interger, float, double)
bolean
array
object
null

*/

$curso = "PHP"; //string
$cargaHoraria = 120; //Integer
$valorCurso = 199.90; //double ou float
$inscricao = true; //boolean
$conteudo = ["Variável", "Função", "POO"]; //array

class Aluno
{
};

$aluno = new Aluno(); //object

$frequencia = null; //null
