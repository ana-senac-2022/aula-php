<?php

function soma($num1, $num2)
{
    $total = $num1 + $num2;

    return $total;
}

function subtracao($num1, $num2)
{
    $total = $num1 - $num2;

    return $total;
}

function multiplicacao($num1, $num2)
{
    $total = $num1 * $num2;

    return $total;
}

function divisao($num1, $num2)
{
    $total = $num1 % $num2;

    return $total;
}

echo soma(10, 9);
echo "<hr>";
echo subtracao(10, 9);
echo "<hr>";
echo multiplicacao(10, 9);
echo "<hr>";
echo divisao(10, 9);
echo "<hr>";

function par_ou_impar($numero)
{
    if ($numero % 2 == 0) {
        return "O numero é par. ";
    }
    return "O numero é impar";
}

echo par_ou_impar(10);
echo "<hr>";

function par_ou_impar_mod2($numero){
    return ($numero % 2 == 0)? "Número é Par" : "Número é Ímpar";
}

echo par_ou_impar_mod2(10);
echo "<hr>";

///////////////////

function geradorSenhaComFor($senhaInicial = 1, $senhaFinal = 10){

    for($contador = $senhaInicial; $contador <= $senhaFinal; $contador++){
        echo $contador, "-";
    }

}

geradorSenhaComFor(10, 20);

echo "<hr>";

function geradorSenhaComWhile($senhaInicial = 1, $senhaFinal = 10){

    $contador = $senhaInicial;

    while($contador <= $senhaFinal){
        echo $contador."-";
        $contador++;
    }

}

geradorSenhaComWhile(10, 20);